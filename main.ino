#include <Servo.h>

int BTN_UP = 2;
int BTN_RIGHT = 3;
int BTN_DOWN = 4;
int BTN_LEFT = 5;
int SERVO_H = 12;
int SERVO_V = 11;
Servo horizontal;
Servo vertical;
int STEP = 5;


bool readButton(int buttonId) {
    return digitalRead(buttonId) < 1;
}


bool upPressed() {
    return readButton(BTN_UP);
}


bool rightPressed() {
    return readButton(BTN_RIGHT);
}


bool downPressed() {
    return readButton(BTN_DOWN);
}


bool leftPressed() {
    return readButton(BTN_LEFT);
}


void setup()
{
    Serial.begin(9600);
    pinMode(BTN_UP, INPUT_PULLUP);
    pinMode(BTN_RIGHT, INPUT_PULLUP);
    pinMode(BTN_DOWN, INPUT_PULLUP);
    pinMode(BTN_LEFT, INPUT_PULLUP);
    horizontal.attach(SERVO_H);
    vertical.attach(SERVO_V);


    horizontal.write(90);
    vertical.write(90);
}


void loop()
{
    Serial.println("------------------");


    if (upPressed()) {
        int current = vertical.read();
        vertical.write(current >= 180 ? 180 : (current + STEP));
    }


    if (rightPressed()) {
        int current = horizontal.read();
        horizontal.write(current <= 0 ? 0 : (current - STEP));
    }


    if (downPressed()) {
        int current = vertical.read();
        vertical.write(current <= 0 ? 0 : (current - STEP));
    }


    if (leftPressed()) {
        int current = horizontal.read();
        horizontal.write(current >= 180 ? 180 : (current + STEP));
    }


    delay(1);
}