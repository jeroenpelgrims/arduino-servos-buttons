# arduino-servos-buttons

This project allows the control of 2 servos (x & y axis) to be controlled using 4 push buttons.  
1 servo is used for the horizontal plane and one is used for the vertical plane.  
Project files created using Fritzing.